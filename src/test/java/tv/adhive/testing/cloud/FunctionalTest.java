package tv.adhive.testing.cloud;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;

public class FunctionalTest {
    @BeforeAll
    public static void setup() {
        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = Integer.valueOf(443);
        }
        else{
            RestAssured.port = Integer.valueOf(port);
        }


        String basePath = System.getProperty("server.base");
        if(basePath==null){
            basePath = "/b/";
        }
        RestAssured.basePath = basePath;

        String baseHost = System.getProperty("server.host");
        if(baseHost==null){
            baseHost = "https://api.jsonbin.io/";
        }
        RestAssured.baseURI = baseHost;

    }

}

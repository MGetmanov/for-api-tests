package tv.adhive.testing.cloud;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class GarageRestTest extends FunctionalTest {
    //https://jsonbin.io/
    @Test
    public void basicPingTest() {
        given().when().get("/5c4fdc17a1021c254839cc92").then().statusCode(200);
    }

    @Test
    public void invalidParkingSpace() {
        given().when().get("/999")
                .then().statusCode(422);
    }

    @Test
    public void verifyNameOfGarage() {
        given().when().get("/5c4fdc17a1021c254839cc92").then()
                .body(containsString("Acme garage"));
    }

    @Test
    public void verifyNameStructured() {
        given().when().get("/5c4fdc17a1021c254839cc92").then()
                .body("name", equalTo("Acme garage"));
    }

    @Test
    public void verifySlotsOfGarage() {
        given().when().get("/5c4fdc17a1021c254839cc92").then().
                body("info.slots", equalTo(150))
                .body("info.status", equalTo("open"));
    }

    @Test
    public void verifyTopLevelURL() {
        given().when().get("/5c4fdc17a1021c254839cc92").then()
                .body("name", equalTo("Acme garage"))
                .body("info.slots", equalTo(150))
                .body("info.status", equalTo("open"))
                .statusCode(200);
    }
}
